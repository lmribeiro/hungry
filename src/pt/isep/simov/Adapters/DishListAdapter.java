package pt.isep.simov.Adapters;

import java.util.List;

import pt.isep.simov.hungry.R;
import pt.isep.simov.hungry.Objects.Dishes;
import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class DishListAdapter extends BaseAdapter {

	List<Dishes> items;

	public DishListAdapter(final Context context, final int itemResId,
			final List<Dishes> items) {
		// TODO Auto-generated constructor stub
		super();
		this.items = items;
	}

	public int getCount() {
		return this.items.size();
	}

	public Object getItem(int arg0) {
		return this.items.get(arg0);
	}

	public long getItemId(int arg0) {
		// TODO Auto-generated method stub
		return 0;
	}

	// arg0 = iD | arg1 = view
	public View getView(int arg0, View arg1, ViewGroup arg2) {
		final Dishes row = this.items.get(arg0);
		View itemView = null;

		if (arg1 == null) {
			LayoutInflater inflater = (LayoutInflater) arg2.getContext()
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

			itemView = inflater.inflate(R.layout.dish_listview_rows,
					null);
		} else {
			itemView = arg1;
		}

		// Set the text of the row
		TextView name = (TextView) itemView.findViewById(R.id.dishName);
		TextView price = (TextView) itemView.findViewById(R.id.price);
		name.setText(row.getName());
		price.setText(Float.toString(row.getPrice()));

		return itemView;
	}

}