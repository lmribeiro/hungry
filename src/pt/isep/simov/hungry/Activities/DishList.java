package pt.isep.simov.hungry.Activities;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import pt.isep.simov.Adapters.DishListAdapter;
import pt.isep.simov.hungry.R;
import pt.isep.simov.hungry.Objects.Dishes;
import pt.isep.simov.hungry.WSCommunications.REST_Asynk;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.JsonHttpResponseHandler;

public class DishList extends ListActivity {
	private int restid;
	private int menuid;
	private String day;
	private String name;
	protected ListView list;
	private DishListAdapter listAdapter;
	private final String uri = ".dish/Find/";
	private ProgressDialog dialog = null;
	private ArrayList<Dishes> dishes = new ArrayList<Dishes>();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_list_dish);

		list = getListView();
		this.restid = getIntent().getIntExtra("restid", 0);
		this.menuid = getIntent().getIntExtra("menuid", 0);
		this.name = getIntent().getStringExtra("restname");
		this.day = getIntent().getStringExtra("day");
		TextView resname = (TextView) findViewById(R.id.textViewRestName);
		resname.setText(this.name);
		System.out.println(name);

		fillListView();
		
		ImageButton reserve = (ImageButton) findViewById(R.id.buttonReserve);
		reserve.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent i = new  Intent(getApplicationContext(), Reservation.class);
				i.putExtra("restid", restid);
				i.putExtra("menuid", menuid);
				i.putExtra("restname", name);
				i.putExtra("day", day);
				startActivity(i);
			}
		});

	}

	/**
	 * Fill the list with the locations
	 */
	@SuppressLint("SimpleDateFormat")
	public void fillListView() {
		final Activity activity = this;

		REST_Asynk.get(uri + menuid, null, new JsonHttpResponseHandler() {
			@Override
			public void onFinish() {
				// TODO Auto-generated method stub
				super.onFinish();
				if (dialog != null) {
					dialog.dismiss();
				}
			}

			@Override
			public void onStart() {
				// TODO Auto-generated method stub
				super.onStart();
				dialog = ProgressDialog.show(DishList.this, "",
						"loading", true);
			}

			@Override
			public void onSuccess(JSONArray json) {
				// Pull the events
				if (json.length() > 0) {
					for (int x = 0; x < json.length(); x++) {
						try {
							JSONObject j = json.getJSONObject(x);
							Dishes d = new Dishes(j.getInt("iddish"), 0, j.getString("name"), Float.parseFloat(j.getString("price")), j.getString("type"));
							dishes.add(d);
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
					listAdapter = new DishListAdapter(getApplicationContext(),
							R.layout.dish_listview_rows, dishes);
					list.setAdapter(listAdapter);
				} else {

					Toast.makeText(getApplicationContext(),
							"No dishes for this menu", Toast.LENGTH_SHORT)
							.show();
					finish();
				}
			}

			@Override
			public void onFailure(Throwable error, String content) {
				System.out.println(content);
				Toast.makeText(activity, "Cant resolve host",
						Toast.LENGTH_LONG).show();
				finish();
			}

		});
	}
}
