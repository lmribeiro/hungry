package pt.isep.simov.hungry.Activities;

import pt.isep.simov.hungry.R;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

public class MainActivity extends Activity {
	final Context context = this;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
            	Intent start = new Intent(getApplicationContext(),
						MainMenu.class);
				startActivityForResult(start, 1);
            }
        }, 2000);
	}

	/**
	 * ! On activity result
	 */
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		switch (resultCode) {
		case RESULT_OK:
			finish();
			break;
		}
	}

}