package pt.isep.simov.hungry.Activities;

import pt.isep.simov.hungry.R;
import pt.isep.simov.hungry.DataBases.DataSource;
import pt.isep.simov.hungry.DataBases.SQLiteHelper;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class MainMenu extends Activity {

	final Context context = this;
	final Activity activity = this;
	private double lat;
	static DataSource dataSource;
	private double lng;
	private int rad = 0;
	private LocationManager service;;
	LocationListener locationListener;
	private boolean enabled;
	private String provider;
	SQLiteHelper dbHelper = new SQLiteHelper(MainMenu.this);

	@SuppressWarnings("static-access")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main_menu);

		this.dataSource = new DataSource(this.getApplicationContext());
		this.service = (LocationManager) getSystemService(LOCATION_SERVICE);
		this.enabled = this.service
				.isProviderEnabled(LocationManager.GPS_PROVIDER);
		// check if enabled and if not send user to the GSP settings
		// Better solution would be to display a dialog and suggesting to
		// go to the settings
		if (!this.enabled) {
			Toast.makeText(
					getApplicationContext(),
					"Hungry requires the location to be active.\nActivate it and press back.",
					Toast.LENGTH_SHORT).show();
			Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
			startActivityForResult(intent, 1);
		}

		// Define a listener that responds to location updates
		locationListener = new LocationListener() {
			public void onLocationChanged(Location location) {
				// Called when a new location is found by the network location
				// provider.
				System.out.println("Location Changed");
				lat = (Double) (location.getLatitude());
				lng = (Double) (location.getLongitude());
			}

			public void onStatusChanged(String provider, int status,
					Bundle extras) {
			}

			public void onProviderEnabled(String provider) {
			}

			public void onProviderDisabled(String provider) {
			}
		};
		// Register the listener with the Location Manager
		// to receive location updates
		service.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0,
				locationListener);

		Button search = (Button) findViewById(R.id.search);
		search.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				final CharSequence[] items = { "5 Km", "10 km", "20 km",
						"Última pesquisa" };
				AlertDialog.Builder builder = new AlertDialog.Builder(context);
				builder.setTitle("Raio de pesquisa");
				builder.setItems(items, new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {

						switch (which) {
						case 0:
							rad = 5;
							getLocation();
							break;
						case 1:
							rad = 10;
							getLocation();
							break;
						case 2:
							rad = 20;
							getLocation();
							break;
						case 3:
							Toast.makeText(context, "Not Implemented",
									Toast.LENGTH_LONG).show();
							break;
						default:
							rad = 5;
							getLocation();
							break;
						}
					}
				});
				AlertDialog alert = builder.create();
				alert.show();
			}
		});

		Button Favoritos = (Button) findViewById(R.id.favorites);
		Favoritos.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Toast.makeText(context, "NOT READY YET!", Toast.LENGTH_LONG)
						.show();
			}
		});
		Button Reservas = (Button) findViewById(R.id.reservations);
		Reservas.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				
				Intent i = new Intent(getApplicationContext(), ShowReservationActivity.class);
				startActivity(i);
			}
		});
	}

	public void getLocation() {
		// Define the criteria how to select the location
		// provider -> use
		// default
		Criteria criteria = new Criteria();
		this.provider = this.service.getBestProvider(criteria, false);
		Location location = this.service.getLastKnownLocation(provider);

		// Initialize the location fields
		if (location != null) {
			System.out.println("Provider " + provider + " has been selected.");
			locationListener.onLocationChanged(location);
		} else {
			System.out.println("2 Tray");
			location = this.service.getLastKnownLocation(provider);
			if (location != null) {
				System.out.println("Provider " + provider
						+ " has been selected.");
				locationListener.onLocationChanged(location);
			}
		}

		Intent i = new Intent(getApplicationContext(), RestaurantList.class);
		i.putExtra("lat", this.lat);
		i.putExtra("lng", this.lng);
		i.putExtra("rad", this.rad);
		startActivity(i);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// Check if location is enable
		this.enabled = service.isProviderEnabled(LocationManager.GPS_PROVIDER);
		// If note show toast
		if (!this.enabled) {
			// Remove comments to finish application
			Toast.makeText(
					getApplicationContext(),
					"Hungry will not work with desable location.\nPlease activate it and restart.",
					Toast.LENGTH_SHORT).show();
			setResult(Activity.RESULT_OK);
			finish();
			
			// Remove comments to send you back to settings
			// Toast.makeText(
			// getApplicationContext(),
			// "Hungry requires the location to be active.\nActivate it and press back.",
			// Toast.LENGTH_SHORT).show();
			// Intent intent = new
			// Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
			// startActivityForResult(intent, 1);

		}
	}

	/**
	 * Change the BackPressed
	 */
	@Override
	public void onBackPressed() {
		setResult(Activity.RESULT_OK);
		finish();
	}

}