package pt.isep.simov.hungry.Activities;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import pt.isep.simov.Adapters.MenuListAdapter;
import pt.isep.simov.hungry.R;
import pt.isep.simov.hungry.Objects.Menus;
import pt.isep.simov.hungry.WSCommunications.REST_Asynk;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.JsonHttpResponseHandler;

@SuppressLint("SimpleDateFormat")
public class MenuList extends ListActivity {
	private int id;
	private String name;
	protected ListView list;
	private Menus menu;
	private MenuListAdapter listAdapter;
	private final String uri = ".menu/";
	private ProgressDialog dialog = null;
	private ArrayList<Menus> menus = new ArrayList<Menus>();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_list_menu);
		list = getListView();
		this.id = getIntent().getIntExtra("id", 0);
		this.name = getIntent().getStringExtra("name");
		TextView resname = (TextView) findViewById(R.id.textViewRestName);
		resname.setText(this.name);
		System.out.println(name);
		fillListView();


		/**
		 * ! Register a callback to be invoked when an item in this AdapterView
		 * has been clicked.
		 */
		list.setOnItemClickListener(new OnItemClickListener() {
			/*
			 * ! Callback method to be invoked when an item in this AdapterView
			 * has been clicked. Implementers can call
			 * getItemAtPosition(position) if they need to access the data
			 * associated with the selected item. Parameters arg0 The
			 * AdapterView where the click happened. arg1 The view within the
			 * AdapterView that was clicked (this will be a view provided by the
			 * adapter) arg2 The position of the view in the adapter. arg3 The
			 * row id of the item that was clicked.
			 */
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				// TODO Auto-generated method stub

				menu = (Menus) listAdapter.getItem(arg2);

				Intent i = new Intent(getApplicationContext(), DishList.class);
				i.putExtra("restid", id);
				SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
				String date = sdf.format(menu.getDay());
				i.putExtra("day",date);
				i.putExtra("menuid", menu.getIdMenu());
				i.putExtra("restname",name );
				startActivityForResult(i, 1);
			}
		});

	}

	/**
	 * ! On activity result
	 */
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		switch (resultCode) {
		case RESULT_OK:
			// this.fillListView();
			break;
		}
	}

	/**
	 * Fill the list with the locations
	 */
	@SuppressLint("SimpleDateFormat")
	public void fillListView() {
		final Activity activity = this;

		REST_Asynk.get(uri + id, null,
				new JsonHttpResponseHandler() {
					@Override
					public void onFinish() {
						// TODO Auto-generated method stub
						super.onFinish();
						if (dialog != null) {
							dialog.dismiss();
						}
					}

					@Override
					public void onStart() {
						// TODO Auto-generated method stub
						super.onStart();
						dialog = ProgressDialog.show(MenuList.this, "",
								"loading", true);
					}

					@Override
					public void onSuccess(JSONArray json) {
						// Pull the events
						if (json.length() > 0) {
							for (int x = 0; x < json.length(); x++) {
								try {
									JSONObject j = json.getJSONObject(x);
									Menus m = new Menus();
									m.setIdMenu(j.getInt("idmenu"));
									SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
									m.setDay(sdf.parse(j.getString("day")));
									menus.add(m);
								} catch (JSONException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								} catch (ParseException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
							}
							listAdapter = new MenuListAdapter(
									getApplicationContext(),
									R.layout.menu_listview_rows, menus);
							// listAdapter.notifyDataSetChanged();
							list.setAdapter(listAdapter);
						} else {

							Toast.makeText(getApplicationContext(), "No menus for this date",
									Toast.LENGTH_SHORT).show();
							finish();
						}
					}

					@Override
					public void onFailure(Throwable error, String content) {
						System.out.println(content);
						Toast.makeText(activity, "Cant resolve host", Toast.LENGTH_LONG)
								.show();
						finish();
					}

				});
	}

}
