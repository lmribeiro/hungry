package pt.isep.simov.hungry.Activities;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import pt.isep.simov.Adapters.DishListAdapter;
import pt.isep.simov.hungry.R;
import pt.isep.simov.hungry.DataBases.DataSource;
import pt.isep.simov.hungry.Objects.Dishes;
import pt.isep.simov.hungry.Objects.Menus;
import pt.isep.simov.hungry.Objects.Reservation_Dish;
import pt.isep.simov.hungry.Objects.Reservations;
import pt.isep.simov.hungry.Objects.ReserveDishe;
import pt.isep.simov.hungry.WSCommunications.REST_Asynk;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.loopj.android.http.JsonHttpResponseHandler;

public class Reservation extends ListActivity {

	private int restid;
	private int menuid;
	private int reserveId;
	private String restname;
	private String day;
	private TimePicker time;
	private EditText people;
	private TextView name;
	private CheckBox takeaway;
	private ArrayList<Dishes> dishes = new ArrayList<Dishes>();
	private ArrayList<ReserveDishe> reserves = new ArrayList<ReserveDishe>();
	protected ListView list;
	private DishListAdapter listAdapter;
	private final String uri = ".dish/Find/";
	private ProgressDialog dialog = null;
	private DataSource dataSource;
	final Context context = this;
	static int qt =2;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_reservation);

		this.dataSource = new DataSource(this.getApplicationContext());
		this.restid = getIntent().getIntExtra("restid", 0);
		this.menuid = getIntent().getIntExtra("menuid", 0);
		this.restname = getIntent().getStringExtra("restname");
		this.day = getIntent().getStringExtra("day");
		time = (TimePicker) findViewById(R.id.timePicker);
		people = (EditText) findViewById(R.id.editTextPeople);
		name = (TextView) findViewById(R.id.textViewReservationRestName);
		name.setText(restname.toString());
		takeaway = (CheckBox) findViewById(R.id.checkBoxTW);
		list = getListView();
		fillListView();

		/**
		 * ! Register a callback to be invoked when an item in this AdapterView
		 * has been clicked.
		 */
		list.setOnItemClickListener(new OnItemClickListener() {
			/*
			 * ! Callback method to be invoked when an item in this AdapterView
			 * has been clicked. Implementers can call
			 * getItemAtPosition(position) if they need to access the data
			 * associated with the selected item. Parameters arg0 The
			 * AdapterView where the click happened. arg1 The view within the
			 * AdapterView that was clicked (this will be a view provided by the
			 * adapter) arg2 The position of the view in the adapter. arg3 The
			 * row id of the item that was clicked.
			 */
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				// TODO Auto-generated method stub
				
				Dishes d = (Dishes) listAdapter.getItem(arg2);

				LayoutInflater layoutInflater = LayoutInflater.from(context);
				View promptView = layoutInflater.inflate(R.layout.dialog, null);
				AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
						context);

				alertDialogBuilder.setView(promptView);
				final EditText input = (EditText) promptView
						.findViewById(R.id.dishQtd);
				// setup a dialog window
				alertDialogBuilder
						.setCancelable(false)

						.setPositiveButton("OK",
								new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog,
											int id) {
										// get user input and set it to result
										Reservation.qt = Integer.parseInt(input.getText().toString());
									}
								})
						.setNegativeButton("Cancelar",
								new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog,
											int id) {
										dialog.cancel();
									}
								});

				// create an alert dialog
				AlertDialog alertD = alertDialogBuilder.create();
				alertD.show();
				ReserveDishe rd = new ReserveDishe(d.getIdDish(),menuid, d.getName(), d.getPrice(), d.getType(), Reservation.qt);
				reserves.add(rd);
			}
		});

		Button reserve = (Button) findViewById(R.id.buttonReservation);
		reserve.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				// TODO Auto-generated method stub
				Reservations r = new Reservations();
				r.setDay(day);
				r.setIdrestaurant(restid);
				r.setName(restname);
				r.setTime(time.getCurrentHour().toString() + ":"
						+ time.getCurrentMinute().toString());
				r.setPeople(Integer.parseInt(people.getText().toString()));
				if (takeaway.isEnabled()) {
					r.setTakeaway(1);
				} else {
					r.setTakeaway(0);
				}
				saveReservationToDatabase(r);

				for (int X = 0; X < reserves.size(); X++) {
					Reservation_Dish rd = new Reservation_Dish();
					ReserveDishe reseve = reserves.get(X);
					rd.setIdreserve(reserveId);
					rd.setIddish(reseve.getIdDish());
					rd.setDishname(reseve.getName());
					rd.setQt(reseve.getQtd());
					saveReservationDishToDatabase(rd);
				}
				
				finish();
			}
		});

	}

	/**
	 * Fill the list with the locations
	 */
	@SuppressLint("SimpleDateFormat")
	public void fillListView() {
		final Activity activity = this;

		REST_Asynk.get(uri + menuid, null, new JsonHttpResponseHandler() {
			@Override
			public void onFinish() {
				// TODO Auto-generated method stub
				super.onFinish();
				if (dialog != null) {
					dialog.dismiss();
				}
			}

			@Override
			public void onStart() {
				// TODO Auto-generated method stub
				super.onStart();
				dialog = ProgressDialog.show(Reservation.this, "", "loading",
						true);
			}

			@Override
			public void onSuccess(JSONArray json) {

				// Pull the events
				if (json.length() > 0) {
					for (int x = 0; x < json.length(); x++) {
						try {
							JSONObject j = json.getJSONObject(x);
							Dishes d = new Dishes(j.getInt("iddish"), 0, j.getString("name"), Float.parseFloat(j.getString("price")), j.getString("type"));
							dishes.add(d);
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
					listAdapter = new DishListAdapter(getApplicationContext(),
							R.layout.dish_listview_rows, dishes);
					list.setAdapter(listAdapter);
				} else {

					Toast.makeText(getApplicationContext(),
							"No dishes for this menu", Toast.LENGTH_SHORT)
							.show();
					finish();
				}
			}

			@Override
			public void onFailure(Throwable error, String content) {
				System.out.println(content);
				Toast.makeText(activity, "Fail to reach the server",
						Toast.LENGTH_LONG).show();
				finish();
			}

		});
	}

	/**
	 * Save reservation into database
	 * 
	 * @param data
	 */
	private void saveReservationToDatabase(Reservations data) {
		dataSource.open();
		dataSource.insertIntoTheDB2(data);
		this.reserveId = dataSource.getLastReservationId();
		dataSource.close();

	}

	private void saveReservationDishToDatabase(Reservation_Dish data) {
		dataSource.open();
		dataSource.insertIntoTheDB3(data);
		dataSource.close();

	}

}
