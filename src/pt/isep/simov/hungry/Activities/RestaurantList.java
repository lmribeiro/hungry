package pt.isep.simov.hungry.Activities;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import pt.isep.simov.Adapters.RestaurantListAdapter;
import pt.isep.simov.hungry.R;
import pt.isep.simov.hungry.DataBases.DataSource;
import pt.isep.simov.hungry.DataBases.SQLiteHelper;
import pt.isep.simov.hungry.Objects.Restaurants;
import pt.isep.simov.hungry.WSCommunications.REST_Asynk;
import android.app.Activity;
import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.Toast;

import com.loopj.android.http.JsonHttpResponseHandler;

public class RestaurantList extends ListActivity {
	private Double lat;
	private Double lng;
	private int rad;
	protected ListView list;
	private Restaurants restaurant;
	private RestaurantListAdapter listAdapter;
	private final String uri = ".restaurant/";
	private ProgressDialog dialog = null;
	private ArrayList<Restaurants> rest = new ArrayList<Restaurants>();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_list_restaurant);
		list = getListView();
		this.lat = getIntent().getDoubleExtra("lat", 0);
		this.lng = getIntent().getDoubleExtra("lng", 0);
		this.rad = getIntent().getIntExtra("rad", 0);

		fillListView();

		/**
		 * ! Register a callback to be invoked when an item in this AdapterView
		 * has been clicked.
		 */
		list.setOnItemClickListener(new OnItemClickListener() {
			/*
			 * ! Callback method to be invoked when an item in this AdapterView
			 * has been clicked. Implementers can call
			 * getItemAtPosition(position) if they need to access the data
			 * associated with the selected item. Parameters arg0 The
			 * AdapterView where the click happened. arg1 The view within the
			 * AdapterView that was clicked (this will be a view provided by the
			 * adapter) arg2 The position of the view in the adapter. arg3 The
			 * row id of the item that was clicked.
			 */
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				// TODO Auto-generated method stub

				restaurant = (Restaurants) listAdapter.getItem(arg2);
				System.out.println(restaurant.getName());

				Intent i = new Intent(getApplicationContext(), MenuList.class);
				i.putExtra("id", restaurant.getIdRestaurant());
				i.putExtra("name", restaurant.getName());
				startActivityForResult(i, 1);
			}
		});

	}

	/**
	 * ! On activity result
	 */
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		switch (resultCode) {
		case RESULT_OK:
			// this.fillListView();
			break;
		}
	}

	/**
	 * Fill the list with the locations
	 */
	public void fillListView() {
		final Activity activity = this;

		REST_Asynk.get(uri + lat + "/" + lng + "/" + rad, null,
				new JsonHttpResponseHandler() {
					@Override
					public void onFinish() {
						// TODO Auto-generated method stub
						super.onFinish();
						if (dialog != null) {
							dialog.dismiss();
						}
					}

					@Override
					public void onStart() {
						// TODO Auto-generated method stub
						super.onStart();
						dialog = ProgressDialog.show(RestaurantList.this, "",
								"loading", true);
					}

					@Override
					public void onSuccess(JSONArray json) {
						// Pull the events
						if (json.length() > 0) {
							for (int x = 0; x < json.length(); x++) {
								try {
									JSONObject j = json.getJSONObject(x);
									Restaurants r = new Restaurants();
									r.setIdRestaurant(j.getInt("idrestaurant"));
									r.setName(j.getString("name"));
									r.setAddress(j.getString("address"));
									r.setDistance(j.getDouble("distance"));
									r.setLatitude(j.getDouble("latitude"));
									r.setLongitude(j.getDouble("longitude"));
									rest.add(r);
								} catch (JSONException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
							}
							saveToDatabase(rest);
							listAdapter = new RestaurantListAdapter(
									getApplicationContext(),
									R.layout.activity_list_menu, rest);
							// listAdapter.notifyDataSetChanged();
							list.setAdapter(listAdapter);
						} else {

							Toast.makeText(getApplicationContext(), "We don't have matchs for your request",
									Toast.LENGTH_SHORT).show();
							finish();
						}
					}

					@Override
					public void onFailure(Throwable error, String content) {
						System.out.println(content);
						Toast.makeText(activity, "Cant resolve host", Toast.LENGTH_LONG)
								.show();
						finish();
					}

				});
	}

	/**
	 * Save the last request to database
	 * @param data
	 */
	private void saveToDatabase(List<Restaurants> data) {
		DataSource SaveLastSearch = new DataSource(this.getApplicationContext());
		SaveLastSearch.open();
		SaveLastSearch.deleteFromTheDB(SQLiteHelper.TABLE_RESTAURANT);
		for (int i = 0; i < data.size(); i++) {
			SaveLastSearch.insertIntoTheDB(data.get(i));
		}
	}

}