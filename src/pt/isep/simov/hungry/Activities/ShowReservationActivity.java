package pt.isep.simov.hungry.Activities;

import java.util.List;

import pt.isep.simov.Adapters.ShowReservationListAdapter;
import pt.isep.simov.hungry.R;
import pt.isep.simov.hungry.DataBases.DataSource;
import pt.isep.simov.hungry.Objects.Reservations;
import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

public class ShowReservationActivity extends ListActivity {

	protected ListView list;
	private DataSource dataSource;
	private ShowReservationListAdapter listAdapter;
//	private Reservations reserve;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_show_reservation);
		this.dataSource = new DataSource(this.getApplicationContext());
		list = getListView();
		fillListView();
		
//		/**
//		 * ! Register a callback to be invoked when an item in this AdapterView
//		 * has been clicked.
//		 */
//		list.setOnItemClickListener(new OnItemClickListener() {
//			/*
//			 * ! Callback method to be invoked when an item in this AdapterView
//			 * has been clicked. Implementers can call
//			 * getItemAtPosition(position) if they need to access the data
//			 * associated with the selected item. Parameters arg0 The
//			 * AdapterView where the click happened. arg1 The view within the
//			 * AdapterView that was clicked (this will be a view provided by the
//			 * adapter) arg2 The position of the view in the adapter. arg3 The
//			 * row id of the item that was clicked.
//			 */
//			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
//					long arg3) {
//				// TODO Auto-generated method stub
//
//				Reservations reserve = (Reservations) listAdapter.getItem(arg2);
//
//				Intent i = new Intent(getApplicationContext(), ShowReservationDish.class);
//				i.putExtra("id", reserve.getId());
//				i.putExtra("name", reserve.getName());
//				i.putExtra("people", reserve.getPeople());
//				i.putExtra("day", reserve.getDay());
//				i.putExtra("time", reserve.getTime());
//				startActivityForResult(i, 1);
//			}
//		});


	}

	private void fillListView() {
		// TODO Auto-generated method stub
		dataSource.open();
		List<Reservations> reserves = dataSource.getAllReservations();
		listAdapter = new ShowReservationListAdapter(getApplicationContext(),
				R.layout.show_reserves_listview_rows, reserves);
		// listAdapter.notifyDataSetChanged();
		this.list.setAdapter(listAdapter);
		dataSource.close();

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.show_reservation, menu);
		return true;
	}

}
