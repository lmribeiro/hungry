package pt.isep.simov.hungry.Activities;

import java.util.List;

import pt.isep.simov.Adapters.ShowReservationDishListAdapter;
import pt.isep.simov.hungry.R;
import pt.isep.simov.hungry.DataBases.DataSource;
import pt.isep.simov.hungry.Objects.Reservation_Dish;
import android.app.ListActivity;
import android.os.Bundle;
import android.widget.ListView;
import android.widget.TextView;

public class ShowReservationDish extends ListActivity {
	
	int id;
	String name;
	int people;
	String day;
	String time;
	private TextView tv_name;
	private TextView tv_people;
	private TextView tv_day;
	private TextView tv_time;
	protected ListView list;
	private DataSource dataSource;
	private ShowReservationDishListAdapter listAdapter;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_show_resservation_dish);
		
		id = getIntent().getIntExtra("id", 0);
		
		people = getIntent().getIntExtra("people", 0);
		day = getIntent().getStringExtra("day");
		name = getIntent().getStringExtra("name");
		time = getIntent().getStringExtra("time");
		this.dataSource = new DataSource(this.getApplicationContext());
		list = getListView();
		
		tv_day = (TextView) findViewById(R.id.TextViewDataDish);
		tv_day.setText(day);
		tv_name  = (TextView) findViewById(R.id.TextViewRestauranteDish);
		tv_name.setText(name);
		tv_people  = (TextView) findViewById(R.id.TextViewPeople);
		tv_people.setText(Integer.toString(people));
		tv_time  = (TextView) findViewById(R.id.TextViewHoraDish);
		tv_time.setText(time);
		
		fillListView();	
	}
	
	private void fillListView() {
		// TODO Auto-generated method stub
		dataSource.open();
		List<Reservation_Dish> reserves = dataSource.getReservationsDishes(id);
		System.out.println(reserves.size());
		listAdapter = new ShowReservationDishListAdapter(getApplicationContext(),
				R.layout.show_reservation_dish_row, reserves);
		// listAdapter.notifyDataSetChanged();
		this.list.setAdapter(listAdapter);
		dataSource.close();

	}


}
