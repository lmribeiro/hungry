package pt.isep.simov.hungry.DataBases;

import java.util.ArrayList;
import java.util.List;

import pt.isep.simov.hungry.Objects.Reservation_Dish;
import pt.isep.simov.hungry.Objects.Reservations;
import pt.isep.simov.hungry.Objects.Restaurants;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

public class DataSource {
	private SQLiteDatabase database;
	private SQLiteHelper dbHelper;
	private String[] allColumns = { SQLiteHelper.COLUMN_IDRESTAURANT,
			SQLiteHelper.COLUMN_NAME, SQLiteHelper.COLUMN_ADDRESS,
			SQLiteHelper.COLUMN_DISTANCE, SQLiteHelper.COLUMN_LATITUDE,
			SQLiteHelper.COLUMN_LONGITUDE };

	private String[] allCollumns2 = { SQLiteHelper.COLUMN_ID,SQLiteHelper.COLUMN_NAME,
			SQLiteHelper.COLUMN_IDREST, SQLiteHelper.COLUMN_DAY,
			SQLiteHelper.COLUMN_TIME, SQLiteHelper.COLUMN_PEOPLE,
			SQLiteHelper.COLUMN_TW };
	
	private String[] allCollumns3 = { SQLiteHelper.COLUMN_IDDISH,SQLiteHelper.COLUMN_IDRESEVATION ,
			SQLiteHelper.COLUMN_QT, SQLiteHelper.COLUMN_QT };

	public DataSource(Context context) {
		dbHelper = new SQLiteHelper(context);
	}

	public void open() throws SQLException {
		database = dbHelper.getWritableDatabase();
	}

	public void close() {
		dbHelper.close();
	}

	public long insertIntoTheDB(Restaurants restaurant) {
		long testResult = -1;
		ContentValues values = new ContentValues();
		values.put(SQLiteHelper.COLUMN_IDRESTAURANT,
				restaurant.getIdRestaurant());
		values.put(SQLiteHelper.COLUMN_NAME, restaurant.getName());
		values.put(SQLiteHelper.COLUMN_ADDRESS, restaurant.getAddress());
		values.put(SQLiteHelper.COLUMN_DISTANCE, restaurant.getDistance());
		values.put(SQLiteHelper.COLUMN_LATITUDE, restaurant.getLatitude());
		values.put(SQLiteHelper.COLUMN_LONGITUDE, restaurant.getLongitude());
		testResult = database.insert(SQLiteHelper.TABLE_RESTAURANT, null,
				values);
		return testResult;
	}

	public long insertIntoTheDB2(Reservations reservation) {
		long testResult = -1;
		ContentValues values = new ContentValues();
		values.put(SQLiteHelper.COLUMN_IDREST, reservation.getIdrestaurant());
		values.put(SQLiteHelper.COLUMN_NAME, reservation.getName());
		values.put(SQLiteHelper.COLUMN_DAY, reservation.getDay());
		values.put(SQLiteHelper.COLUMN_TIME, reservation.getTime());
		values.put(SQLiteHelper.COLUMN_PEOPLE, reservation.getPeople());
		values.put(SQLiteHelper.COLUMN_TW, reservation.getTakeaway());
		testResult = database.insert(SQLiteHelper.TABLE_RESERVATION, null,
				values);
		return testResult;
	}

	public long insertIntoTheDB3(Reservation_Dish rd) {
		long testResult = -1;
		ContentValues values = new ContentValues();
		values.put(SQLiteHelper.COLUMN_IDRESEVATION, rd.getIdreserve());
		values.put(SQLiteHelper.COLUMN_IDDISH, rd.getIddish());
		values.put(SQLiteHelper.COLUMN_DISHNAME, rd.getDishname());
		values.put(SQLiteHelper.COLUMN_QT, rd.getQt());
		testResult = database.insert(SQLiteHelper.TABLE_RESERVATION_DISH, null,
				values);
		return testResult;
	}

	public void deleteFromTheDB(String arg) {
		database.delete(arg, null, null);
	}

	public void deleteFromTheDB2(String arg) {
		database.delete(arg, null, null);
	}

	public List<Restaurants> getAllRestaurants() {
		List<Restaurants> restaurants = new ArrayList<Restaurants>();
		Cursor cursor = database.query(SQLiteHelper.TABLE_RESTAURANT,
				allColumns, null, null, null, null, null);
		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			Restaurants r = new Restaurants();
			r.setId(cursor.getInt(cursor.getColumnIndex(SQLiteHelper.COLUMN_ID)));
			r.setIdRestaurant(cursor.getInt(cursor
					.getColumnIndex(SQLiteHelper.COLUMN_IDRESTAURANT)));
			r.setName(cursor.getString(cursor
					.getColumnIndex(SQLiteHelper.COLUMN_NAME)));
			r.setAddress(cursor.getString(cursor
					.getColumnIndex(SQLiteHelper.COLUMN_ADDRESS)));
			r.setDistance(cursor.getFloat(cursor
					.getColumnIndex(SQLiteHelper.COLUMN_DISTANCE)));
			r.setLatitude(cursor.getFloat(cursor
					.getColumnIndex(SQLiteHelper.COLUMN_LATITUDE)));
			r.setLongitude(cursor.getFloat(cursor
					.getColumnIndex(SQLiteHelper.COLUMN_LONGITUDE)));
			restaurants.add(r);
			cursor.moveToNext();
		}
		cursor.close();
		return restaurants;
	}

	public List<Reservations> getAllReservations() {
		List<Reservations> reservations = new ArrayList<Reservations>();
		Cursor cursor = database.query(SQLiteHelper.TABLE_RESERVATION,
				allCollumns2, null, null, null, null, null);
		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			Reservations r = new Reservations();
			r.setId(cursor.getInt(cursor.getColumnIndex(SQLiteHelper.COLUMN_ID)));
			r.setIdrestaurant(cursor.getInt(cursor
					.getColumnIndex(SQLiteHelper.COLUMN_IDRESTAURANT)));
			r.setName(cursor.getString(cursor
					.getColumnIndex(SQLiteHelper.COLUMN_NAME)));
			r.setDay(cursor.getString(cursor
					.getColumnIndex(SQLiteHelper.COLUMN_DAY)));
			r.setTime(cursor.getString(cursor
					.getColumnIndex(SQLiteHelper.COLUMN_TIME)));
			r.setPeople(cursor.getInt(cursor
					.getColumnIndex(SQLiteHelper.COLUMN_PEOPLE)));
			r.setTakeaway(cursor.getInt(cursor
					.getColumnIndex(SQLiteHelper.COLUMN_TW)));
			reservations.add(r);
			cursor.moveToNext();
		}
		cursor.close();
		return reservations;
	}
	
	
	public List<Reservation_Dish> getReservationsDishes(int id) {
		List<Reservation_Dish> reservations = new ArrayList<Reservation_Dish>();
		Cursor cursor = database.rawQuery("SELECT * FROM " + SQLiteHelper.TABLE_RESERVATION_DISH + " WHERE " + SQLiteHelper.COLUMN_IDRESEVATION + "=" + id, null);
		System.out.println("Cursor: "+cursor.getCount());
		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			Reservation_Dish r = new Reservation_Dish();
			r.setIddish(cursor.getInt(cursor.getColumnIndex(SQLiteHelper.COLUMN_IDDISH)));
			r.setIdreserve(cursor.getInt(cursor
					.getColumnIndex(SQLiteHelper.COLUMN_IDRESEVATION)));
			r.setQt(cursor.getInt(cursor
					.getColumnIndex(SQLiteHelper.COLUMN_QT)));
			r.setDishname(cursor.getString(cursor
					.getColumnIndex(SQLiteHelper.COLUMN_DISHNAME )));
			reservations.add(r);
			cursor.moveToNext();
		}
		cursor.close();
		return reservations;
	}

	public int getLastReservationId() {
		int id = 0;
		Cursor cursor = database.query(SQLiteHelper.TABLE_RESERVATION,
				allCollumns2, null, null, null, null, null);
		cursor.moveToLast();
		id = cursor.getInt(cursor.getColumnIndex(SQLiteHelper.COLUMN_ID));
		cursor.close();
		return id;
	}
}
