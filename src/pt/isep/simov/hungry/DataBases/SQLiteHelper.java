package pt.isep.simov.hungry.DataBases;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class SQLiteHelper extends SQLiteOpenHelper{
	private static final String DATABASE_NAME = "hungry.db";
	private static final int DATABASE_VERSION = 1;
	
	public static final String TABLE_RESTAURANT = "restaurant";
	public static final String COLUMN_ID = "id";
	public static final String COLUMN_IDRESTAURANT = "idrestaurant";
	public static final String COLUMN_NAME = "name";
	public static final String COLUMN_ADDRESS = "address";
	public static final String COLUMN_DISTANCE = "distance";
	public static final String COLUMN_LATITUDE = "latitude";
	public static final String COLUMN_LONGITUDE = "longitude";
	
	public static final String TABLE_RESERVATION = "reservation";
	public static final String COLUMN_IDREST = "idrestaurant";
	public static final String COLUMN_IDMENU = "idmenu";
	public static final String COLUMN_PEOPLE = "people";
	public static final String COLUMN_TW = "take_away";
	public static final String COLUMN_DAY = "day";
	public static final String COLUMN_TIME = "time";
	
	public static final String TABLE_RESERVATION_DISH = "reservation_dish";
	public static final String COLUMN_IDDISH = "iddish";
	public static final String COLUMN_IDRESEVATION = "idreservation";
	public static final String COLUMN_QT = "qt";
	public static final String COLUMN_DISHNAME = "dish_name";
	
	
	private static final String DATABASE_CREATE_RESTAURANT = "create table " + TABLE_RESTAURANT + "(" 
			+ COLUMN_ID + " INTEGER primary key autoincrement, "
			+ COLUMN_IDRESTAURANT + " INTEGER not null, "
			+ COLUMN_NAME + " TEXT not null, "
			+ COLUMN_ADDRESS + " TEXT not null, "
			+ COLUMN_DISTANCE + " REAL not null, "
			+ COLUMN_LATITUDE + " REAL not null, "
			+ COLUMN_LONGITUDE + " REAL not null)";
	
	private static final String DATABASE_CREATE_RESERVATION = "create table " + TABLE_RESERVATION + "(" 
			+ COLUMN_ID + " INTEGER primary key autoincrement, "
			+ COLUMN_IDREST + " INTEGER not null, "
			+ COLUMN_NAME + " TEXT not null, "
			+ COLUMN_DAY + " TEXT not null, "
			+ COLUMN_TIME + " TEXT not null, "
			+ COLUMN_PEOPLE + " INTEGER not null, "
			+ COLUMN_TW + " INTEGER not null)";
	
	
	private static final String DATABASE_CREATE_RESERVATION_DISH = "create table " + TABLE_RESERVATION_DISH + "(" 
			+ COLUMN_IDDISH + " INTEGER not null, "
			+ COLUMN_IDRESEVATION + " INTEGER not null, "
			+ COLUMN_QT + " INTEGER not null, "
			+ COLUMN_DISHNAME + " TEXT not null)";
	
	public SQLiteHelper(Context context){
		super(context , DATABASE_NAME , null , DATABASE_VERSION);				
	}
	

	@Override
	public void onCreate(SQLiteDatabase database) {
		// TODO Auto-generated method stub
		database.execSQL(DATABASE_CREATE_RESTAURANT);
		database.execSQL(DATABASE_CREATE_RESERVATION);
		database.execSQL(DATABASE_CREATE_RESERVATION_DISH);
	}
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_RESTAURANT);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_RESERVATION);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_RESERVATION_DISH);
		onCreate(db);
	}

	public static String getDatabaseCreateReservationDish() {
		return DATABASE_CREATE_RESERVATION_DISH;
	}

}
