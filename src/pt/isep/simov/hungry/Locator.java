package pt.isep.simov.hungry;

import android.content.Context;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.app.Activity;

public class Locator {

	public static Location locator(Activity activity){

		LocationManager locationManager;
		String provider;
		locationManager = (LocationManager) activity.getSystemService(Context.LOCATION_SERVICE);
		Criteria criteria = new Criteria();

		provider = locationManager.getBestProvider(criteria, true);
		Location location = locationManager.getLastKnownLocation(provider);
		
		return location;
	}

}
