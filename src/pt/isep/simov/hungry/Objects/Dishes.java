package pt.isep.simov.hungry.Objects;

public class Dishes {
	private int iddish;
	private int idmenu;
	private String name;
	private float price;
	private String type;
	
	

	public Dishes(int iddish, int idmenu, String name, float price, String type) {
		super();
		this.iddish = iddish;
		this.idmenu = idmenu;
		this.name = name;
		this.price = price;
		this.type = type;
	}
	public int getIdDish(){
		return iddish;
	}
	public void setIdDish(int iddish){
		this.iddish = iddish;
	}
	
	public int getIdMenu(){
		return idmenu;
	}
	public void setIdMenu(int idmenu){
		this.idmenu = idmenu;
	}
	
	public String getName(){
		return name;
	}
	public void setName(String name){
		this.name = name;
	}
	
	public float getPrice(){
		return price;
	}
	public void setPrice(float price){
		this.price = price;
	}
	
	public String getType(){
		return type;
	}
	public void setType(String type){
		this.type = type;
	}
	
}