package pt.isep.simov.hungry.Objects;

import java.util.Date;

public class Menus {
	private int idmenu;
	private int idrestaurant;
	private Date day;
	private String restaurantname;

	
	public int getIdMenu(){
		return idmenu;
	}
	public void setIdMenu(int idmenu){
		this.idmenu = idmenu;
	}
	
	public int getIdRestaurant(){
		return idrestaurant;
	}
	public void setIdRestaurant(int idrestaurant){
		this.idrestaurant = idrestaurant;
	}
	
	public Date getDay(){
		return day;
	}
	public void setDay(Date day){
		this.day = day;
	}

	public String getRestaurantname() {
		return restaurantname;
	}
	public void setRestaurantname(String restaurantname) {
		this.restaurantname = restaurantname;
	}
	
}
