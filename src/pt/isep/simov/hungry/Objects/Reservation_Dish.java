package pt.isep.simov.hungry.Objects;

public class Reservation_Dish {

	private int idreserve;
	private int iddish;
	private String dishname;
	private int qt;
	
	public int getIdreserve() {
		return idreserve;
	}
	public void setIdreserve(int idreserve) {
		this.idreserve = idreserve;
	}
	public int getIddish() {
		return iddish;
	}
	public void setIddish(int iddish) {
		this.iddish = iddish;
	}
	public String getDishname() {
		return dishname;
	}
	public void setDishname(String dishname) {
		this.dishname = dishname;
	}
	public int getQt() {
		return qt;
	}
	public void setQt(int qt) {
		this.qt = qt;
	}
	
	

}
