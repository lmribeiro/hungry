package pt.isep.simov.hungry.Objects;

import java.util.ArrayList;
import java.util.List;

public class Reservations {
	private int id;
	private int idreservation = 0;
	private List<Dishes> dishes = new ArrayList<Dishes>();
	private int idrestaurant;
	private String name;
	private int takeaway;
	private String day;
	private String time;
	private int people;
	
	public int getTakeaway() {
		return takeaway;
	}
	public List<Dishes> getDishes() {
		return dishes;
	}
	public void setDishes(List<Dishes> dishes) {
		this.dishes = dishes;
	}
	public int getPeople() {
		return people;
	}
	public void setPeople(int people) {
		this.people = people;
	}
	public void setTakeaway(int takeaway) {
		this.takeaway = takeaway;
	}
	public void addDish(Dishes d) {
		this.dishes.add(d);
		
	}
	public Dishes getDish(int id) {
		Dishes dish = null;
		for (int x=0; x<this.dishes.size(); x++){
			Dishes d = this.dishes.get(x);
			if (d.getIdDish() == id){
				dish = d;
			}
		}
		return dish;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getIdreservation() {
		return idreservation;
	}
	public void setIdreservation(int idreservation) {
		this.idreservation = idreservation;
	}

	public int getIdrestaurant() {
		return idrestaurant;
	}
	public void setIdrestaurant(int idrestaurant) {
		this.idrestaurant = idrestaurant;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDay() {
		return day;
	}
	public void setDay(String day) {
		this.day = day;
	}
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}
	

	
}
