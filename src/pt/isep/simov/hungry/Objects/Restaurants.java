package pt.isep.simov.hungry.Objects;

public class Restaurants {
	private int id;
	private int idrestaurant;
	private String name;
	private String address;
	private double distance;
	private double latitude;
	private double longitude;
	
	public int getId(){
		return id;
	}
	public void setId(int id){
		this.id = id;
	}
	
	public int getIdRestaurant(){
		return idrestaurant;
	}
	public void setIdRestaurant(int idrestaurant){
		this.idrestaurant = idrestaurant;
	}
	
	public String getName(){
		return name;
	}
	public void setName(String name){
		this.name = name;
	}
	
	public String getAddress(){
		return address;
	}
	public void setAddress(String address){
		this.address = address;
	}
	
	public double getDistance(){
		return distance;
	}
	public void setDistance(double distance){
		this.distance = distance;
	}
	
	public double getLatitude(){
		return latitude;
	}
	public void setLatitude(double latitude){
		this.latitude = latitude;
	}
	
	public double getLongitude(){
		return longitude;
	}
	public void setLongitude(double longitude){
		this.longitude = longitude;
	}
	@Override
	public String toString() {
		
		return "Restaurante ID: "+ this.idrestaurant; 
	}
	
	
}
