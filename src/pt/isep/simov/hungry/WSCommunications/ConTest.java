package pt.isep.simov.hungry.WSCommunications;

import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.widget.Toast;

public class ConTest {
	public static Context context;

	public static void testConn(Activity activity, Context context) {
		ConnectivityManager connMgr = (ConnectivityManager) activity
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo networkInfo = connMgr
				.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
		boolean isWifiConn = networkInfo.isConnected();
		networkInfo = connMgr.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
		boolean isMobileConn = networkInfo.isConnected();
		Toast.makeText(context, "wi " + isWifiConn + " mob " + isMobileConn,
				Toast.LENGTH_SHORT).show();
		if (isWifiConn == false && isMobileConn == false) {
			Toast.makeText(context, "Ligue o Wifi/3G", Toast.LENGTH_SHORT)
					.show();
		}
	}

	public static boolean isOnline(Activity activity) {
		ConnectivityManager connMgr = (ConnectivityManager) activity
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
		return (networkInfo != null && networkInfo.isConnected());
	}

}
