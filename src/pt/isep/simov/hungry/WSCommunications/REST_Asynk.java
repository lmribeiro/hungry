package pt.isep.simov.hungry.WSCommunications;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

public class REST_Asynk {

	// public static String BASE_URL =
	// "http://Porto:8080/HungryServer/webresources/hungryserver";

	public static String BASE_URL = "http://172.31.21.194:8080/HungryServer/webresources/hungryserver";
	private static AsyncHttpClient client = new AsyncHttpClient();

	public static void get(String url, RequestParams params,
			AsyncHttpResponseHandler responseHandler) {
		client.setTimeout(8000);
		System.out.println(getAbsoluteUrl(url));
		client.get(getAbsoluteUrl(url), params, responseHandler);

	}

	public static void post(String url, RequestParams params,
			AsyncHttpResponseHandler responseHandler) {
		client.setTimeout(8000);
		System.out.println(getAbsoluteUrl(url));
		client.post(getAbsoluteUrl(url), params, responseHandler);
	}

	private static String getAbsoluteUrl(String relativeUrl) {
		return BASE_URL + relativeUrl;

	}
}
